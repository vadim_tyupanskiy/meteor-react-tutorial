import { Meteor } from 'meteor/meteor';

import React from 'react';
import { render } from 'react-dom';

import Card from '../../ui/components/Card';

class App extends React.Component {
    render() {
        return (
            <div className="application">
                <Card/>
            </div>
        );
    }
}

Meteor.startup(() => {
    render(<App />, document.getElementById('app'));
});


