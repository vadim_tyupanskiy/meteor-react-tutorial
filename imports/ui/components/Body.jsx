import React from 'react';

export default class Body extends React.Component {
    render() {
        return (
            <p>{this.props.content}</p>
        );
    }
}

