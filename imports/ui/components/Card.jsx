import React from 'react';

import Header from './Header';
import Body from './Body';
import Button from './Button';

export default class Card extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: 'No title',
            content: `I am a very simple card. I am good at containing small bits of information.
                                I am convenient because I require little markup to use effectively.`,
            text1: 'Title 1',
            text2: 'Title 2'
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(title) {
        this.setState({title: title});
        alert('Set title to ' + title);
    }

    render() {
        return (
            <div className="row">
                <div className="col s12 m6">
                    <div className="card blue-grey darken-1">
                        <div className="card-content white-text">
                            <Header title={ this.state.title }/>
                            <Body content={ this.state.content }/>
                        </div>
                        <div className="card-action">
                            <Button text={ this.state.text1 } handleClick={this.handleClick} />
                            <Button text={ this.state.text2 } handleClick={this.handleClick} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}