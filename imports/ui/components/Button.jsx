import React from 'react';

export default class CardButton extends React.Component {
    constructor(props){
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.handleClick(this.props.text);
        console.log('click!');
    }

    render() {
        return (
            <a href="#" onClick={ this.handleClick }>{ this.props.text }</a>
        );
    }
}
